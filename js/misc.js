(function ($) {
  Drupal.behaviors.injectLogoSVG = {
    attach: function (context, settings) {
      if (isIE()) return
      var mySVGsToInject = document.querySelectorAll('img.inject-me');
      SVGInjector(mySVGsToInject, {
        evalScripts: 'never',
        each: function (el) {
          el.classList.add('svg-injected')
        }
      })
    }
  };

  if ($('.path-checkout').length) {
    $(document).scroll(function() {
      $('.layout-region-checkout-secondary').css({"transform": "translate3d(0px, " + $(document).scrollTop()*0.7 + "px, 0px)"})
    })

    Drupal.behaviors.autoCalculateShipping = {
      attach: function (context, settings) {
        $('input.postal-code', context).blur(recalculateShipping)
        $('select.locality', context).blur(recalculateShipping)
        $('select.administrative-area', context).blur(recalculateShipping)
      }
    };

    Drupal.behaviors.removeCouponAutocomplete = {
      attach: function (context, settings) {
        $('.form-item-sidebar-coupon-redemption-form-code input', context).attr('autocomplete', 'false')
      }
    };

    Drupal.behaviors.syncAdress = {
      attach: function (context, settings) {
        // checkbox injection
        const syncLabelText = 'O endereço de cobraça é o mesmo de entrega'
        const $syncForms = $('[data-drupal-selector=edit-payment-information-add-payment-method]')
          .add('[data-drupal-selector=edit-payment-information-billing-information]')
        $syncForms.once('sync')
          .prepend('<span class="sync-wrapper"><input id="sync-checkbox" type="checkbox" /><label for="sync-checkbox">'+syncLabelText+'</label></span>')
          .find('#sync-checkbox').change(function() { $syncForms.find('> div:not(.credit-card-form)').toggle() }).click()

        //submit handling
        $('.commerce-checkout-flow-multistep-default #edit-actions-next').once('sync').mousedown(function() {
          if ($('#sync-checkbox').is(":checked")) {
            syncCheckoutForms()
          }
        })
      }
    }
  }

  function isIE() {
      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");
      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
          return true
      }
      return false;
  }

  function syncCheckoutForms() {
    var $sCpf = jQuery('.checkout-pane-shipping-information .field--type-cpf input')
    var $sTel = jQuery('.checkout-pane-shipping-information .field--type-telephone input')
    var $sFName = jQuery('.checkout-pane-shipping-information .given-name')
    var $sLName = jQuery('.checkout-pane-shipping-information .family-name')
    var $sAddr1 = jQuery('.checkout-pane-shipping-information .address-line1')
    var $sAddr2 = jQuery('.checkout-pane-shipping-information .address-line2')
    var $sLocality = jQuery('.checkout-pane-shipping-information .locality')
    var $sAdmArea = jQuery('.checkout-pane-shipping-information .administrative-area')
    var $sPostalCode = jQuery('.checkout-pane-shipping-information .postal-code')

    var $bCpf = jQuery('.checkout-pane-payment-information .field--type-cpf input')
    var $bTel = jQuery('.checkout-pane-payment-information .field--type-telephone input')
    var $bFName = jQuery('.checkout-pane-payment-information .given-name')
    var $bLName = jQuery('.checkout-pane-payment-information .family-name')
    var $bAddr1 = jQuery('.checkout-pane-payment-information .address-line1')
    var $bAddr2 = jQuery('.checkout-pane-payment-information .address-line2')
    var $bLocality = jQuery('.checkout-pane-payment-information .locality')
    var $bAdmArea = jQuery('.checkout-pane-payment-information .administrative-area')
    var $bPostalCode = jQuery('.checkout-pane-payment-information .postal-code')

    $bCpf.val($sCpf.val())
    $bTel.val($sTel.val())
    $bFName.val($sFName.val())
    $bLName.val($sLName.val())
    $bAddr1.val($sAddr1.val())
    $bAddr2.val($sAddr2.val())
    $bLocality.val($sLocality.val())
    $bAdmArea.val($sAdmArea.val())
    $bPostalCode.val($sPostalCode.val())
  }

  function recalculateShipping() {
    if($('select.locality').length > 0 ){
      $('input[id*=edit-shipping-information-recalculate-shipping]').mousedown()
    }
  }

})(jQuery);
