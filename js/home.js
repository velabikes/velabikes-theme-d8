(function ($) {
  var $body = $('body');

  if ($body.hasClass('path-frontpage')) {

    //About Title Scrolling
    $(window).scroll(function () {
      var $this = $(this);
      var $scrollTop = $this.scrollTop();
      var $aboutTitle = $('#about h2');
      if ($scrollTop > 150 && !$aboutTitle.hasClass('about-title-scrolled')) {
        $aboutTitle.addClass('about-title-scrolled');
      }
      if ($scrollTop < 120 && $aboutTitle.hasClass('about-title-scrolled')) {
        $aboutTitle.removeClass('about-title-scrolled');
      }
      var scrollBot = $scrollTop+window.innerHeight
      var $moreInfo = $('#more-info');
      var $slider = $('#features');
      if (scrollBot > $moreInfo.offset().top+240 && !$moreInfo.hasClass('scrolled')) {
        let sliderHeight = $slider.height()+400;
        $moreInfo.css('transform', 'translate(0,'+sliderHeight+'px)');
        $moreInfo.addClass('scrolled');
      }
      if ($scrollTop < 300 && $moreInfo.hasClass('scrolled')) {
        $moreInfo.css('transform', 'translate(0,0)')
        $moreInfo.removeClass('scrolled');
      }

    });

    //Video Sizing
    var $video = $('#vela-homepage-video')
    var wW = window.innerWidth
    var wH = window.innerHeight
    if (wW > 600) {
      $video.height(wH-70);
    } else {
      $video.height(wH/2.2);
    }
  }
})(jQuery)
