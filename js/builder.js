
// Get the Object by ID
var modal = document.querySelector('.builder');
if (modal !== null) {
  var backBtn = document.querySelector('.modal-back');
  var modalBtn = document.querySelector('.modal-toggle');
  var nextBtn = document.querySelector('.modal-next');
  
  var heightField = document.querySelector('div[class^=form-item-field-height]')
  var armField = document.querySelector('div[class^=form-item-field-arm]')
  var legField = document.querySelector('div[class^=form-item-field-let]')
  
  var variations = document.querySelector('div.field--widget-commerce-product-variation-attributes')
  
  jQuery(modalBtn).click(function() {
    jQuery(modal).removeClass('hidden').attr('build-step', 'measurement')
  })
  
  jQuery(nextBtn).click(function() {
    var step = jQuery(modal).attr('build-step')
    if (step == 'measurement') {
      jQuery(modal).attr('build-step', 'color')
    }
    if (step == 'color') {
      jQuery(modal).attr('build-step', 'model')
    }
    
  })
  
  jQuery(backBtn).click(function() {
    var step = jQuery(modal).attr('build-step')
    if (step == 'measurement') {
      jQuery(modal).addClass('hidden')
    }
    if (step == 'color') {
      jQuery(modal).attr('build-step', 'measurement')
    }
    if (step == 'model') {
      jQuery(modal).attr('build-step', 'color')
    }
  })
}