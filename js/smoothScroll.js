Drupal.behaviors.smoothScroll = {
  attach: function (context, settings) {
    jQuery('a[href*=\\#]').click(function(){
        var $this = jQuery(this);
        var url = $this.attr('href')
        var hash = url.substring(url.indexOf("#"))
        jQuery('html, body').animate({
            scrollTop: jQuery( hash ).offset().top-140
        }, 500);
        return false;
    });
  }
}