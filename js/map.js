(function ($) {
  Drupal.behaviors.partnersMap = {
    attach: function (context, settings) {
      const maps = $('.partners-map', context)
      var options = {
        evalScripts: 'once',
        pngFallback: 'assets/png',
        each: svg => {
          const regionsWithPartnersPaths = regionsWithPartners()
            .map(region => $(`#${region}`))

          regionsWithPartnersPaths.forEach(path => path.addClass('filled'))

          $paths = $(svg).find('g > *')
          $paths.each(function () {
            $(this).on('click', function () {
              $current = ''

              if ($(this).attr('id') !== 'br') {
                $current = $(this).attr('id')
              }

              $otherPaths = $paths.filter(function () {
                return $(this).attr('id') !== $current
              })

              if ($otherPaths.length < 6) {
                $otherPaths.removeClass('map-active')
              }

              $(this).addClass('map-active')
              switch ($current) {
                case 's':
                  $('.Sul.views-row').css('display', 'inline-block')
                  $('.Sudeste.views-row').hide()
                  $('.Norte.views-row').hide()
                  $('.Centro-Oeste.views-row').hide()
                  $('.Nordeste.views-row').hide()
                  break
                case 'co':
                  $('.Sul.views-row').hide()
                  $('.Sudeste.views-row').hide()
                  $('.Norte.views-row').hide()
                  $('.Centro-Oeste.views-row').css('display', 'inline-block')
                  $('.Nordeste.views-row').hide()
                  break
                case 'se':
                  $('.Sul.views-row').hide()
                  $('.Sudeste.views-row').css('display', 'inline-block')
                  $('.Norte.views-row').hide()
                  $('.Centro-Oeste.views-row').hide()
                  $('.Nordeste.views-row').hide()
                  break
                case 'ne':
                  $('.Sul.views-row').hide()
                  $('.Sudeste.views-row').hide()
                  $('.Norte.views-row').hide()
                  $('.Centro-Oeste.views-row').hide()
                  $('.Nordeste.views-row').css('display', 'inline-block')
                  break
                case 'n':
                  $('.Sul.views-row').hide()
                  $('.Sudeste.views-row').hide()
                  $('.Norte.views-row').css('display', 'inline-block')
                  $('.Centro-Oeste.views-row').hide()
                  $('.Nordeste.views-row').hide()
                  break
              }
            })
          })
        }
      }
      SVGInjector(maps, options)
    }
  }

  const regionsWithPartners = () => {
    const partners = $('.partners-block-content .views-row')

    let partnersClasses = []
    partners.each(function () {
      partnersClasses.unshift($(this).attr('class').split(' ')[0])
    })

    partnersClassesIds = partnersClasses.map(className => regionsMap(className))

    return partnersClassesIds
      .filter((id, i, ids) => ids.indexOf(id) === i)
  }

  const regionsMap = region => {
    let regionId = ''
    switch (region) {
      case 'Sul':
        regionId = 's'
        break;
      case 'Sudeste':
        regionId = 'se'
        break;
      case 'Centro-Oeste':
        regionId = 'co'
        break;
      case 'Norte':
        regionId = 'n'
        break;
      case 'Nordeste':
        regionId = 'ne'
        break;
      default:
        regionId = ''
    }
    return regionId
  }
})(jQuery)
